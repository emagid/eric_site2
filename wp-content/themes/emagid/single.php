<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package emagid
 */

get_header(); ?>

	<!-- HERO SECTION -->
	<!-- <section class='hero home_hero' style="background-image:url(<?php the_field('banner'); ?>)"> -->
	<section class='hero home_hero' id="desktop_version">
        <div class="overlay">
            <div class='text_box'>
                <div class="inner">
                    <h1 style="font-size: 60px;"><?php the_title(); ?></h1>
                </div>
			</div>
			<div class='image_box'>
                <div class="inner">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/eric_2_fade.png" alt="">
                </div>
            </div>
        </div>
    </section>
    <section class='hero home_hero' id="mobile_version" style="display: none;">
        <div class="overlay">
            <div class='whole_box'>
                <div class="inner">
                    <h1 style="font-size: 60px;"><?php the_title(); ?></h1>
                    <br/>
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/eric_2_fade.png" alt="">
                </div>
			</div>
        </div>
	</section>

	<!-- HERO SECTION END -->

<div class="blog_container">
	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php
		while ( have_posts() ) : the_post();

			get_template_part( 'template-parts/content', get_post_type() );

			the_post_navigation();

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- #primary -->
</div>
<?php
get_footer();
