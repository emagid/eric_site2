<?php
/**
 * The template for displaying general page
 *
 *  Template Name: Inner Template
 * 
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package emagid
 */

get_header(); ?>

	<!-- HERO SECTION -->
	<!-- <section class='hero home_hero' style="background-image:url(<?php the_field('banner'); ?>)"> -->
	<section class='hero home_hero' id="desktop_version">
        <div class="overlay">
            <div class='text_box'>
                <div class="inner">
                    <h1 style="font-size: 60px;"><?php the_title(); ?></h1>
                    <p>Eric Schleien</p>
                </div>
			</div>
			<div class='image_box'>
                <div class="inner">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/eric_2_fade.png" alt="">
                </div>
            </div>
        </div>
    </section>

	<!-- HERO SECTION END -->

	

	<section class="content_inner">
		<h2><strong><?php the_field('content_title'); ?></strong></h2>
		<p><?php the_field('content'); ?></p>
	</section>

	<!-- END CONTENT SECTION -->
	
	

<?php
get_footer();
