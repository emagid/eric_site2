<?php
/**
 * The template for displaying all pages
 * 
 *  Template Name: Investments Page
 * 
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress constr<?php
/**
 * The template for displaying all pages
 *
 *  Template Name: About Page
 * 
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package emagid
 */

get_header(); ?>


	<!-- HERO SECTION -->
	<section class='hero home_hero' style="background-image:url(<?php the_field('banner'); ?>)">
        <div class="overlay">
            <div class='text_box'>
                <h1>INVESTMENTS</h1>
            </div>
        </div>
	</section>

	<!-- HERO SECTION END -->


    <section class='bio' id="investments_bio">


        <!-- <div class="image-wrapper">
            <button>VIEW CASE STUDY</button>
            <img class="img_small" src="<?php the_field('top-right'); ?>">
        </div> -->

        <div class="container image-wrapper">
            <div class="content">
                <a href="/case-studies" target="_blank">
                <div class="content-overlay"></div>
                <img class="content-image" src="<?php the_field('top-left'); ?>">
                <div class="content-details fadeIn-top">
                    <button>VIEW CASE STUDY</button>
                </div>
                </a>
            </div>
        </div>

        <div class="container image-wrapper">
            <div class="content">
                <a href="/case-studies" target="_blank">
                <div class="content-overlay"></div>
                <img class="content-image" src="<?php the_field('top-right'); ?>">
                <div class="content-details fadeIn-top">
                    <button>VIEW CASE STUDY</button>
                </div>
                </a>
            </div>
        </div>

        <div class="container image-wrapper bottom_row">
            <div class="content">
                <a href="/case-studies" target="_blank">
                <div class="content-overlay"></div>
                <img class="content-image" src="<?php the_field('bottom-left'); ?>">
                <div class="content-details fadeIn-top">
                    <button>VIEW CASE STUDY</button>
                </div>
                </a>
            </div>
        </div>

        <div class="container image-wrapper bottom_row">
            <div class="content">
                <a href="/case-studies" target="_blank">
                <div class="content-overlay"></div>
                <img class="content-image" src="<?php the_field('bottom-right'); ?>">
                <div class="content-details fadeIn-top">
                    <button>VIEW CASE STUDY</button>
                </div>
                </a>
            </div>
        </div>


        <!-- <div class="image-wrapper">
            <button>VIEW CASE STUDY</button>
            <img class="img_big" src="<?php the_field('top-left'); ?>">
        </div>

        <div class="image-wrapper">
            <button>VIEW CASE STUDY</button>
            <img class="img_small" src="<?php the_field('top-right'); ?>">
        </div>

        <div class="image-wrapper bottom_row">
            <button>VIEW CASE STUDY</button>
            <img class="img_small" src="<?php the_field('bottom-left'); ?>">
        </div>

        <div class="image-wrapper bottom_row">
            <button>VIEW CASE STUDY</button>
            <img class="img_big" src="<?php the_field('bottom-right'); ?>"> 
        </div> -->

        <div class="talk_button">
            <div>
                <a href="/contact">
                    <button class='lets_talk button'> LETS TALK </button>
                </a>
            </div>
        </div>

	</section>


<?php
get_footer();