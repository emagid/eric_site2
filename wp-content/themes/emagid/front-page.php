<?php
/**
 * The template for displaying front page
 *
 *  Template Name: Front Page
 * 
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package emagid
 */

get_header(); ?>

	<!-- HERO SECTION -->
	<!-- <section class='hero home_hero' style="background-image:url(<?php the_field('banner'); ?>)"> -->
	<section class='hero home_hero' id="desktop_version">
        <div class="overlay">
            <div class='text_box'>
                <div class="inner">
                    <h1 style="font-size: 60px;">Eric<br/>Schleien</h1>
                    <p><?php the_field('banner_text'); ?></p>
                    <a href='/about' class='button '> LEARN MORE </a>
                </div>
			</div>
			<div class='image_box'>
                <div class="inner">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/eric_2_fade.png" alt="">
                </div>
            </div>
        </div>
    </section>
    <section class='hero home_hero' id="mobile_version" style="display: none;">
        <div class="overlay">
            <div class='whole_box'>
                <div class="inner">
                    <h1 style="font-size: 60px;">Eric<br/>Schleien</h1>
                    <p><?php the_field('banner_text'); ?></p>
                    <a href='/contact' class='button'> LEARN MORE </a><br/>
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/eric_2_fade.png" alt="Eric Schleien">
                </div>
			</div>
        </div>
	</section>

	<!-- HERO SECTION END -->

	<!-- MEMBERSHIP SECTION -->
	<section id="membership">
	<h2>MEMB<span>ERSHIP GR</span>OUPS</h2>
	<div class="slider-content">
        
            <?php
	  			$args = array(
	    		'post_type' => 'member_groups'
	    		);
	  			$products = new WP_Query( $args );
	  				if( $products->have_posts() ) {
	    			while( $products->have_posts() ) {
	      		$products->the_post();
			?>
        
		<section class='membership'>	
			<img src="<?php the_field('logo'); ?>">
		</section>
        
        <?php
			}
				}
			else {
			echo 'No Member Groups Found';
			}
		?>  
	
<?php wp_reset_query();	 // Restore global post data stomped by the_post(). ?>
	</div>
	</section>
	<!-- MEMBERSHIP END  -->


	<!-- CONTENT SECTION -->

	<section class="content_home">
		<h2><strong><?php the_field('content_title'); ?></strong></h2>
        <p><?php the_field('content'); ?></p>
        <div style="text-align: center;"><a href='/about' class='button'> READ MORE </a></div><br/>
	</section>

	<!-- END CONTENT SECTION -->
	
	<!-- BIO SECTION -->
    <section class='bio' id="investments_bio">

        <div class="container image-wrapper iw-1">
            <div class="content">
                <a href="<?php the_field('top_left_link'); ?>" >
                <div class="content-overlay"></div>
                <img class="content-image" src="<?php the_field('top-left'); ?>" alt="STATE REPRESENTATIVE">
                <div class="content-details fadeIn-top">
                    <button>STATE REPRESENTATIVE</button>
                </div>
                </a>
            </div>
        </div>

        <div class="container image-wrapper iw-2">
            <div class="content">
				<a href="<?php the_field('top_right_link'); ?>" >
                <div class="content-overlay"></div>
                <img class="content-image" src="<?php the_field('top-right'); ?>" alt="PERSONAL COACH">
                <div class="content-details fadeIn-top">
                    <button>PERSONAL COACH</button>
                </div>
                </a>
            </div>
        </div>

        <div class="container image-wrapper bottom_row bottom_row_left iw-3">
            <div class="content">
				<a href="<?php the_field('bottom_left_link'); ?>" >
                <div class="content-overlay"></div>
                <img class="content-image" src="<?php the_field('bottom-left'); ?>" alt="PODCASTER">
                <div class="content-details fadeIn-top">
                    <button>PODCASTER</button>
                </div>
                </a>
            </div>
        </div>

        <div class="container image-wrapper bottom_row iw-4">
            <div class="content">
				<a href="<?php the_field('bottom_right_link'); ?>">
                <div class="content-overlay"></div>
                <img class="content-image" src="<?php the_field('bottom-right'); ?>" alt="INVESTOR">
                <div class="content-details fadeIn-top">
                    <button>INVESTOR</button>
                </div>
                </a>
            </div>
        </div>


        <div class="cta_button">
            <div>
                <a href="/hobbies">
                    <button class='lets_talk button'> AND MUCH MORE </button>
                </a>
            </div>
        </div>

	</section>

	<!-- END BIO SECTION -->

  <script type="text/javascript">
    $(document).ready(function(){
      $('.slider-content').slick({
        autoplay:true,
		autoplaySpeed:3500,
		dots: true,
		infinite: true,
		slidesToShow: 1,
		slidesToScroll: 1,
        centerMode: true,
        variableWidth: true,
        responsive: [{
        breakpoint: 1024,
        settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
            infinite: true,
            dots: true
        }
    }, {
        breakpoint: 600,
        settings: {
            slidesToShow: 2,
            slidesToScroll: 2
        }
    }, {
        breakpoint: 480,
        settings: {
            slidesToShow: 1,
            slidesToScroll: 1
        }
    }]
      });
    });
  </script>
  

<?php
get_footer();
