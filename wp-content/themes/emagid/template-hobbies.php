<?php
/**
 * The template for displaying Hobbies page
 *
 *  Template Name: Hobbies Page
 * 
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package emagid
 */

get_header(); ?>

	<!-- HERO SECTION -->
	<!-- <section class='hero home_hero' style="background-image:url(<?php the_field('banner'); ?>)"> -->
	<section class='hero home_hero' id="desktop_version">
        <div class="overlay">
            <div class='text_box'>
                <div class="inner">
                    <h1 style="font-size: 60px;"><?php the_title(); ?></h1>
                    <p>Eric Schleien</p>
                </div>
			</div>
			<div class='image_box'>
                <div class="inner">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/eric_2_fade.png" alt="">
                </div>
            </div>
        </div>
    </section>
    <section class='hero home_hero' id="mobile_version" style="display: none;">
        <div class="overlay">
            <div class='whole_box'>
                <div class="inner">
                    <h1 style="font-size: 60px;"><?php the_title(); ?></h1>
                    <p>Eric Schleien</p><br/>
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/eric_2_fade.png" alt="">
                </div>
			</div>
        </div>
	</section>

	<!-- HERO SECTION END -->

<section class="hobbies">
    <div class="row">
        <img src="<?php the_field('image_1'); ?>" alt="">
        <div class="para right_element float_me"><?php the_field('text_1'); ?></div>
    </div>
    <div class="row">
        <img class="right_element float_me" src="<?php the_field('image_2'); ?>" alt="">    
        <div class="para"><?php the_field('text_2'); ?></div>
        
    </div>
    <div class="row">
        <img src="<?php the_field('image_3'); ?>" alt="">
        <div class="para right_element float_me"><?php the_field('text_3'); ?></div>
    </div>
    <div class="row">
        <img class="right_element float_me" src="<?php the_field('image_4'); ?>" alt="">
        <div class="para"><?php the_field('text_4'); ?></div>
    </div>
    <div class="row">
        <img src="<?php the_field('image_5'); ?>" alt="">
        <div class="para right_element float_me"><?php the_field('text_5'); ?></div>
    </div>

</section>

<?php
get_footer();
