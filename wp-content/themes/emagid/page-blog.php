<?php

/**
 * The template for displaying Hobbies page
 *
 *  Template Name: Posts
 * 
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package emagid
 */
get_header(); ?>


	<!-- HERO SECTION -->
	<!-- <section class='hero home_hero' style="background-image:url(<?php the_field('banner'); ?>)"> -->
	<section class='hero home_hero' id="desktop_version">
        <div class="overlay">
            <div class='text_box'>
                <div class="inner">
                    <h1 style="font-size: 60px;"><?php the_title(); ?></h1>
                    <p>Eric Schleien</p>
                </div>
			</div>
			<div class='image_box'>
                <div class="inner">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/eric_2_fade.png" alt="">
                </div>
            </div>
        </div>
    </section>
    <section class='hero home_hero' id="mobile_version" style="display: none;">
        <div class="overlay">
            <div class='whole_box'>
                <div class="inner">
                    <h1 style="font-size: 60px;"><?php the_title(); ?></h1>
                    <p>Eric Schleien</p><br/>
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/eric_2_fade.png" alt="">
                </div>
			</div>
        </div>
	</section>

	<!-- HERO SECTION END -->

<div class="blog_container">
    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <header class="entry-header">
            <?php
            if ( is_singular() ) :
                the_title( '<h1 class="entry-title">', '</h1>' );
            else :
                the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
            endif;

            if ( 'post' === get_post_type() ) : ?>
            <div class="entry-meta">
                <?php
                    emagid_posted_on();
                    emagid_posted_by();
                ?>
            </div><!-- .entry-meta -->
            <?php
            endif; ?>
        </header><!-- .entry-header -->

        <?php emagid_post_thumbnail(); ?>

        <div class="entry-content">
            <?php
                the_content( sprintf(
                    wp_kses(
                        /* translators: %s: Name of current post. Only visible to screen readers */
                        __( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'emagid' ),
                        array(
                            'span' => array(
                                'class' => array(),
                            ),
                        )
                    ),
                    get_the_title()
                ) );

                wp_link_pages( array(
                    'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'emagid' ),
                    'after'  => '</div>',
                ) );
            ?>
        </div><!-- .entry-content -->

    </article><!-- #post-<?php the_ID(); ?> -->
</div>


<?php
get_footer();
