<?php
/**
 * The template for displaying Press page
 *
 *  Template Name: Press Page
 * 
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package emagid
 */

get_header(); ?>
	<!-- HERO SECTION -->
	<!-- <section class='hero home_hero' style="background-image:url(<?php the_field('banner'); ?>)"> -->
	<section class='hero home_hero' id="desktop_version">
        <div class="overlay">
            <div class='text_box'>
                <div class="inner">
                    <h1 style="font-size: 60px;">Eric<br/>Schleien</h1>
<!--                    <p>Senator | Investor | Opportunist</p>-->
                    <p><?php the_field('banner_text'); ?></p>
                    <a href='/about' class='button'> LEARN MORE </a>
                </div>
			</div>
			<div class='image_box'>
                <div class="inner">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/eric_2_fade.png" alt="Eric Schleien">
                </div>
            </div>
        </div>
    </section>
    <section class='hero home_hero' id="mobile_version" style="display: none;">
        <div class="overlay">
            <div class='whole_box'>
                <div class="inner">
                    <h1 style="font-size: 60px;">Eric<br/>Schleien</h1>
                    <p>Senator | Investor | Opportunist</p>
                    <a href='/contact' class='button'> LEARN MORE </a><br/>
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/eric_2_fade.png" alt="Eric Schleien">
                </div>
			</div>
        </div>
	</section>

	<!-- HERO SECTION END -->
 <section class="press_links">     
                                <?php
	  			$args = array(
	    		'post_type' => 'press_links',
                'posts_per_page' => '25'
	    		);
	  			$products = new WP_Query( $args );
	  				if( $products->have_posts() ) {
	    			while( $products->have_posts() ) {
	      		$products->the_post();
			?> 

        <div class="press">
            <div class="content">
                <a href="<?php the_field('link'); ?>" target="_blank">
                    <h3><?php the_field('title'); ?></h3>
                    <?php if( get_field('image') ): ?>
                <img class="content-image" src="<?php the_field('image'); ?>">
                    <?php endif; ?>
                    <div class="media">
                        <?php the_field('media'); ?>
                    </div>
                    
                </a>
            </div>
        </div>
        
        <?php
			}
				}
			else {
			echo 'No Clients Found';
			}
		?>  

</section>  

<?php
get_footer();
