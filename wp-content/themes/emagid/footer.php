<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package emagid
 */

?>

	<footer>
		<nav class="links">
            <?php
                    wp_nav_menu( array(
                        'theme_location' => 'menu-1'
                    ) );
                ?>
		</nav>
        
		<nav class="social">
            <a href="" target="_blank">
			     <img style="padding-right: 10px;" src="/wp-content/uploads/2018/03/fb_icon.png" alt="Facebook"> 
            </a>
            <a href="https://twitter.com/iinvesting1" target="_blank">
			     <img style="padding-right: 10px;" src="/wp-content/uploads/2018/03/twitter_icon.png" alt="Twitter"> 
            </a>
            <a href="https://www.linkedin.com/in/eric-schleien-594155108/" target="_blank">
			     <img src="/wp-content/uploads/2018/03/twitter_icon_1.png" alt="LinkedIn"> 
            </a>
            
		</nav>
        
                				<div class='newsletter'>
					<p>SIGN UP FOR EXCLUSIVE UPDATES</p>

                    <div class="newsletter_form">
                        <?php echo do_shortcode('[contact-form-7 id="214" title="Newsletter"]'); ?>
                    </div>
                
				</div>
</footer>
        

	
	<!-- FOOTER SECTION ENDS -->
        

<?php wp_footer(); ?>

</body>
</html>
