<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'ericschleien');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '1234');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '2j=ilmwl{p31pJ]gXa3yg|CI|(7wQ8NfOqASVRq[bqTh=<>FsKCC!cE,m{_sh8)#');
define('SECURE_AUTH_KEY',  'V0PGP?f3l8ipuA1(&iFsCX1Vj48R/<2Aqk/S?Ink21ob{gQoi)NSpzuNHI.b.;$ ');
define('LOGGED_IN_KEY',    '=ZE1hMbT]#Pj)RK8i)zx?R#Z 5ok{Q>J7Uc[(yYQI^l^@@}fA:z:h}6^<|c7gJI;');
define('NONCE_KEY',        'bd*kW=w+,m3T/|(L9hG<&9[X5U;+uV$nS$wsHs06DsP)7vq3R?NQCmmF%0Rc{+#}');
define('AUTH_SALT',        'RU]jEXC_L;,^t67d34^ q#}Sz2Fj8Q?`yT3i$q/C3zi9@eO^UH cQk> CN)$fVrO');
define('SECURE_AUTH_SALT', 'How8A4Oshj7$AbY|Oox.WrwePIKy|l](|~)6R%G<}C@Di)3J78b-KcSGs/d+t/^N');
define('LOGGED_IN_SALT',   '/z%g*t!_}*Q<m-|Y:>{BLL v*b3kz[.Nm^I}gX^H@k(9;1`@@3+!>[s^.VBM1SlO');
define('NONCE_SALT',       '#$d=FQ>>)+}{`,BD:NnaoGf[B(|I&RYN64YnN1-EwW7iU2UH2;L/,/]MJB{G lGN');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
